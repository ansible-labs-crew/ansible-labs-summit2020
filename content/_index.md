+++
title = "2020"
weight = 100
+++
# Red Hat Summit 2020

[Ansible Getting Started](./ansible-getting-started/)

[Ansible Tower Getting Started](./ansible-tower-getting-started/)

[Ansible Tower Advanced](./ansible-tower-advanced/)

[Ansible Collections](./ansible-collections/)

[Visual Studio Code - Introduction](./vscode-intro)
